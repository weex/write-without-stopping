# write-without-stopping

Try writing without stopping or editing. No backspace and with beeps if you stop.

Operates without tracking or sending data anywhere. 

At the end just copy what you've written somewhere more permanent.

Demo: https://davidsterry.com/write-without-stopping/